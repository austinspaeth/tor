// BROWSER DETECTION //
export function TorBrowser(){

var opr;
var safari;

if((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0){return 'opera';}
if(typeof InstallTrigger !== 'undefined'){return 'firefox';}
if(/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification))){return 'safari';}
if(/*@cc_on!@*/false || !!document.documentMode){return 'ie';} var isIE = /*@cc_on!@*/false || !!document.documentMode;
if(!isIE && !!window.StyleMedia){return 'edge';}
if(!!window.chrome && !!window.chrome.webstore){return 'chrome';}

}

// OPERATING SYSTEM DETECTION //
export function TorOS(){

var userAgent = window.navigator.userAgent,platform = window.navigator.platform,macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],iosPlatforms = ['iPhone', 'iPad', 'iPod'],os = null;

if (macosPlatforms.indexOf(platform) !== -1) {return 'mac';}
if (iosPlatforms.indexOf(platform) !== -1) {return 'ios';}
if (windowsPlatforms.indexOf(platform) !== -1) {return 'windows';}
if (/Android/.test(userAgent)) {return 'android';}
if (!os && /Linux/.test(platform)) {return 'linux';}

}

// COOKIE DETECTION //
export function TorCookies(){

var cookieEnabled = navigator.cookieEnabled;
if (!cookieEnabled){ document.cookie = "testcookie";cookieEnabled = document.cookie.indexOf("testcookie")!=-1;}
return cookieEnabled || false;

}