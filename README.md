# Tor

This is a script that will detect multiple compatibility issues in your project including:

* Browser Detection
* OS Detection (Windows, MacOS, Linux, iOS, Android)
* Cookies Disabled

Using the instructions below, you can easily detect any compatibility issue and deal with it as you see fit. 


## Installation

Installation is crazy easy! Just type:

```
npm install https://gitlab.com/austinspaeth/tor.git
```

## Usage

Let's check for compatability issues! Below is an example of how to implement Tor into your app:

```
import {TorBrowser, TorOS, TorCookies, TorImages} from 'tor';

componentDidMount(){
if(TorBrowser() == 'chrome'){alert('Chrome is detected');}
if(TorOS() == 'mac'){alert('MacOS is detected');}
if(TorCookies()){alert('Cookies are enabled);} else {alert('Cookies are disabled');}
}
```

#### TorBrowser
This method returns the detected browser. It will return `edge`, `chrome`, `safari`, `firefox`, `opera` or `ie`.

#### TorOS
This method returns the detected operating system. It will return `mac`, `windows`, `linux`, `android` or `ios`.

#### TorCookies
This method returns whether cookies are disabled or not. It will return with either `true` or `false`.